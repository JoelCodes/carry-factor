
clientData = {};


if (Meteor.isClient) {
    Meteor.methods({
        getSummoner: (summonerName, key) => {


            HTTP.get("https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/" + summonerName + "?api_key=" + key,
                {
                    param: {}
                }, function (error, result) {
                    if (!error) {


                        var summonerNamee = summonerName.split(' ').join('');

                        var noSpacesSummoner = summonerNamee.toLowerCase().trim();
                        clientData.pubName = summonerName;
                        clientData.name = noSpacesSummoner;
                        clientData.id = result.data[noSpacesSummoner].id;
                        clientData.lvl = result.data[noSpacesSummoner].summonerLevel;

                        console.log(clientData.id);

                        Meteor.call("getGames", clientData.id, key);

                    }
                });
        },


        champImg: (champId, key) => {
            HTTP.get("https://global.api.pvp.net/api/lol/static-data/na/v1.2/champion/" + champId + "?champData=image&api_key=" + key,
                {
                    param: {}
                }, function (error, result) {
                    if (!error) {
                        var champ = result.data.image.full;
                        var string = "http://ddragon.leagueoflegends.com/cdn/6.2.1/img/champion/" + champ;
                        console.log(string);


                    }
                })
        },
        getGames: (summonerId, key) => {

            HTTP.get("https://na.api.pvp.net/api/lol/na/v1.3/game/by-summoner/" + summonerId + "/recent?api_key=" + key,
                {
                    param: {}
                }, function (error, result) {
                    if (!error) {
                        //Used for Winstreak
                        let winArr = [],
                        //Champs Played in last 10 games
                            champIdArr = [],
                        //Used for KDA
                            killsArr = [],
                            assistArr = [],
                            deathArr = [],
                        //Misc Bonuses
                            longSpreeArr = [],
                            wardsPlacedArr = [],
                            goldFarmedArr = [],
                            killingSpreesArr = [];


                        result.data.games.map(function (games) {
                            winArr.push(games.stats.win);
                            champIdArr.push(games.championId);
                            if (games.stats.championsKilled === undefined) {
                                killsArr.push(1);
                            } else {
                                killsArr.push(games.stats.championsKilled);
                            }

                            if (games.stats.assists === undefined) {
                                assistArr.push(1);
                            } else {
                                assistArr.push(games.stats.assists);
                            }

                            if (games.stats.numDeaths === undefined) {
                                deathArr.push(1);
                            } else {
                                deathArr.push(games.stats.numDeaths);
                            }

                            if (games.stats.largestKillingSpree === undefined) {
                                longSpreeArr.push(1);
                            } else {
                                longSpreeArr.push(games.stats.largestKillingSpree);
                            }

                            if (games.stats.wardPlaced === undefined) {
                                wardsPlacedArr.push(4);
                            } else {
                                wardsPlacedArr.push(games.stats.wardPlaced);
                            }

                            if (games.stats.goldEarned) {
                                goldFarmedArr.push(10000);
                            } else {
                                goldFarmedArr.push(games.stats.goldEarned);
                            }


                            if (games.stats.killingSprees === undefined) {
                                killingSpreesArr.push(1);
                            } else {
                                killingSpreesArr.push(games.stats.killingSprees);
                            }
                        });


                        function CalcCarryFactor(winArr, killsArr, assistArr, deathArr, longSpreeArr, wardsPlacedArr, goldFarmedArr, killingSpreesArr) {
                            function sumArr(arr) {
                                //var total = 0;
                                //
                                //for (var i = 0; i < arr.length; ++i) {
                                //    total += arr[i]
                                //}
                                //console.log("the sum of" + total)

                                return arr.reduce( (prevVal, currVal) => prevVal + currVal )
                            }

                            var STREAK = 0;

                            for (var i = 0; i < winArr.length; i++) {
                                var firstGame = winArr[0];

                                if (winArr[i] === firstGame) {
                                    STREAK++
                                } else {
                                    if (firstGame === true) {
                                        console.log(STREAK);
                                        break;
                                    } else {
                                        STREAK = -Math.abs(STREAK)
                                        console.log(STREAK)
                                        break;
                                    }

                                }
                            } //end of STREAK FORLOOP
                            var KDA = (sumArr(killsArr) + sumArr(assistArr) / 2) - sumArr(deathArr);
                            var SPREE = sumArr(longSpreeArr) + sumArr(wardsPlacedArr) + sumArr(goldFarmedArr) / 9000 + sumArr(killingSpreesArr) / 1.5;

                            clientData.kills = sumArr(killsArr);
                            clientData.assist = sumArr(assistArr);
                            clientData.death = sumArr(deathArr);
                            clientData.streak = STREAK;


                            return Math.round(((KDA + SPREE) * STREAK));
                        } //END OF FUNCTION

                        var carryFactor = CalcCarryFactor(winArr, killsArr, assistArr, deathArr, longSpreeArr, wardsPlacedArr, goldFarmedArr, killingSpreesArr);

                        clientData.carryFactor = carryFactor;
                        clientData.createAt = new Date();

                        console.log(clientData);
                        console.log("Your CarryFactor is " + carryFactor);
                        CarryFactorData.insert(clientData);

                        ReactLayout.render(Remake);


                        //Creating the FrontEnd


                    }
                })
        }


    });

}
