LandingPage = React.createClass({

    css : {
        fullbleed: {
            background: "black",
            color: "white",
            width: "70%",
            opacity: 1,
            position: "relative",
            paddingBottom: "10px"
        },

        matches: {
            background: "black",
            color: "white",
            width: "70%",
            height: "auto",
            opacity: 1,
            position: "relative",
            display: "flex",
            margin: "0 auto"
        },

        wrapper: {
            position: "relative",
            top: "200px"
        }
    },


    getInitialState() {
      return {
          search: "hello"
      }
    },

    render() {
        return (

          <div style={this.css.wrapper}>

              <div style={this.css.fullbleed} className="row">
                  <div className="col s4">
                      <input  ref="input" placeholder="Summoner Name"/>
                      <button onClick={this.click} className="btn-floating btn-large waves-effect waves-light red">Feed</button>


                  </div>
                  <div className="col s4">
                      <h6 id="lvl">{this.state.search}</h6>
                  </div>
                  <div className="col s4">
                      <h6 id="id"></h6>
                  </div>
              </div>
              <div className="center-align" style={this.css.matches} id="matches">
              </div>


          </div>
        );
    },

    mixins: [ReactMeteorData],

    getMeteorData() {

        return {
            key : Key.find().fetch()[0]
        };
    },


    click() {
        var sumName = ReactDOM.findDOMNode(this.refs.input).value
        if(sumName === " ") {
            console.log("No name found")
        } else {


        var clientD =  Meteor.call("getSummoner", sumName,this.data.key.key);
            console.log(clientD)




        }

    },

    componentDidMount() {

    }

    });