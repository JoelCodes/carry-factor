Score = React.createClass({

    css: {
        img: {
            height: "30px",
            width: "30px"
        },
        stats: {
            width: "20%",
            marginTop: "10px",
            marginRight: "auto",
            marginLeft: "auto"
        }
    },



    render() {
        return (
                <div style={this.css.stats} className="center" id="stats row">
                        <img style={this.css.img} className="responsive-img" src="/rivenicon.png" />
                    <br />
                        <span>{clientData.pubName}</span>
                    <br />
                        <span>Carry Factor:  </span>
                        <span>{clientData.carryFactor}</span>
                    <br />
                        <span>(Last 10 game summary)</span>
                    <br />
                   <div>Kills: <span>{clientData.kills}</span></div>
                    <div>Deaths: <span>{clientData.death}</span></div>
                    <div>Assist: <span>{clientData.assist}</span></div>
                    <div>Streak: <span>{clientData.streak}</span></div>


                </div>

        )


    },

    componentDidMount() {
        $(".center").hide().fadeIn()
    }
});