//I need a fullbleed, a input bar

Remake = React.createClass({
    css : {
        fullbleed: {
            background: "url('/img1.jpg') no-repeat center center fixed",
            width: "100%",
            height: "50vh",
            color: "white",
            paddingTop: "200px"



        },

        scoreboard: {
            height: "50vh",
            width: "100%"
        },

        input: {
            width: "40%",
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            background: "rgba(255,255,255,0.1)",
            border: "whitesmoke 1px solid"
        }
    },

    mixins: [ReactMeteorData],

    getMeteorData() {

        return {
            key : Key.find().fetch()[0]
        };
    },


    render() {
        return (
            <section>
                <h1 className="center-align">Carry Factor</h1>
                <div style={this.css.fullbleed}>
                    <input style={this.css.input} id="input" onKeyDown={this.enter}  ref="input" placeholder="Summoner Name"/>
                </div>
                <div id="scoreboard"  style={this.css.scoreboard}>

                    {this.getScores()}

                </div>
            </section>
            )

    },

    enter(e) {
        if (e.keyCode === 13) {

            var $scoreNode = $("#scoreboard");
            var $stats = $("#stats");
            $scoreNode.fadeOut("fast");

            var sumName = ReactDOM.findDOMNode(this.refs.input).value;

            Meteor.call("getSummoner", sumName, this.data.key.key);

            ReactDOM.findDOMNode(this.refs.input).value = "";


            $scoreNode.fadeIn(2000);

            $("html, body").animate({
                scrollTop: "100px"
            },2000)







        }
    },

    getScores() {
        return <Score name="Joel" />
    },

    componentDidMount() {
        $("input").focus()
    }



});